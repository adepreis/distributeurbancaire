
import banque.Carte;
import banque.Compte;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Classe DistributeurBancaire. Permet d'effectuer les tests de validation du programme.
 * 4 Cas sont testés : Déroulement normal, Blocage d'une carte, Retrait partiel, Distributeur hors service.
 * @author Antonin
 */
public class DistributeurBancaire {

    /**
     * Méthode main. Permet de manipuler le distributeur et d'effectuer des tests.
     * @param args the command line arguments
     */
    public static void main(String[] args){        
        
        Distributeur distributeur = new Distributeur(5000);    //Distributeur avec 10k€ en fond initial
        
        
        Compte compte1 = new Compte(2500);  //Compte avec 2500€
        
        Carte carte1 = new Carte("9876", compte1);    //Carte du client possedant le compte1 / code
        
        
        System.out.println("========= Cas n°1 : Consultation solde et retrait sans \"incident\" : =========");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On insere la carte :");
        distributeur.insererCarte(carte1);
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On saisit le code :");
        distributeur.saisirCode("9876");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On consulte le solde :");
        distributeur.consulterSolde();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On effectue un retrait :");
        distributeur.effectuerRetrait();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On retire la somme de 1200€ :");
        distributeur.retirerBillets(1200);
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On consulte le solde :");
        distributeur.consulterSolde();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On retire la carte :");
        distributeur.retirerCarte();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        // pour tester le bon fonctionnement
        
        
        
        System.out.println("\n\n========= Cas n°2 : Blocage d'une carte suite à 3 échecs : =========");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On insere la carte :");
        distributeur.insererCarte(carte1);
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On saisit le code :");
        distributeur.saisirCode("0000");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On saisit le code :");
        distributeur.saisirCode("1234");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On saisit le code :");
        distributeur.saisirCode("2468");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On insere la carte qui a été avalée :");
        distributeur.insererCarte(carte1);
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        
        //Création d'un compte de test secondaire avec sa carte
        Compte compte2 = new Compte(5500);  //Compte avec 5500€
        
        Carte carte2 = new Carte("2018", compte2);    //Carte du client possedant le compte2 / code "2018"
        
        
        System.out.println("\n\n========= Cas n°3 : Consultation solde et retrait partiel : =========");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On insere la carte :");
        distributeur.insererCarte(carte2);
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On saisit le code :");
        distributeur.saisirCode("2018");
        distributeur.afficherEtat();  //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On consulte le solde :");
        distributeur.consulterSolde();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On effectue un retrait :");
        distributeur.effectuerRetrait();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On retire la somme de 4300€ :");
        distributeur.retirerBillets(4300);
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On consulte le solde :");
        distributeur.consulterSolde();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        System.out.println("\n  • On retire la carte :");
        distributeur.retirerCarte();
        distributeur.afficherEtat();    //-> Pour verifier l'état actuel du distributeur
        
        
        
        System.out.println("\n\n========= Cas n°4 : Distributeur Hors Service : =========");
        distributeur.afficherEtat();
        
        System.out.println("\n  • On insere la carte :");
        try {
            // J'ai mis un timer de 50ms pour que le message d'erreur s'affiche au "bon moment" lors de l'exécution
            Thread.sleep(50);
            distributeur.insererCarte(carte2);
        } catch (IllegalStateException e) {
            System.err.println(e.getMessage());
        } catch (InterruptedException ex) {
            Logger.getLogger(DistributeurBancaire.class.getName()).log(Level.SEVERE, null, ex);
        }
        distributeur.afficherEtat();
    }
    
}

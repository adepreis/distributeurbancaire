import banque.*;

/**
 * Classe EnAttenteOperation. Implémente l'état où le distributeur attend un choix d'opération.
 * @author Antonin
 */
public class EnAttenteOperation implements Etat {

	private Distributeur monDistributeur;

	/**
	 * Constructeur de la classe EnAttenteOperation.
	 * @param unDistributeur
	 */
	public EnAttenteOperation(Distributeur unDistributeur) {
            this.monDistributeur = unDistributeur;
	}

        @Override
	public void insererCarte(Carte uneCarte) {
            throw new IllegalStateException("Vous ne pouvez pas insérer plus d'une carte dans le distributeur.");
	}

	/**
	 * 
	 * @param unCode
	 */
        @Override
	public void saisirCode(String unCode) {
            throw new IllegalStateException("Votre code est déjà saisi, veuillez choisir une opération a effectuer.");
	}

        @Override
	public void retirerBillets(int somme) {
            throw new IllegalStateException("Vous ne pouvez pas retirer de billets avant de sélectionner l'opération \"Effectuer un Retrait\".");
	}

        @Override
	public Carte retirerCarte() {
            
            //gérer le changement d'état (2 CHOIX POSSIBLES EN Fct des fonds restants) :
            if (this.monDistributeur.estVide()) {
                this.monDistributeur.setEtat(this.monDistributeur.getEtatHorsService());
            }else{
                this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteCarte());
            }
            
            this.monDistributeur.setCarte(null); //???
            return this.monDistributeur.getCarte(); //???
        }

        @Override
	public int consulterSolde() {
            //ne pas changer d'état
            System.out.println("Solde : " + this.monDistributeur.getCarte().getCompte().getSolde() + "€");
            return this.monDistributeur.getCarte().getCompte().getSolde();
        }

        @Override
	public void reapprovisionner(int somme) {
            throw new IllegalStateException("Vous ne pouvez pas réapprovisionner le distributeur car une carte est insérée.");
	}

        @Override
	public void effectuerRetrait() {
            
            //si le distributeur n'est pas vide :
            if (!this.monDistributeur.estVide()) {
                //et qu'il y a de l'argent dispo sur le compte :
                if (this.monDistributeur.getCarte().getCompte().getSolde() > 0) {
                    this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteMontant());
                }
            }
	}

        @Override
	public String getMessage() {
            return "→ Etat distributeur : " + this.getClass().getSimpleName();
	}

}
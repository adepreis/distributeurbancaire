import banque.*;

/**
 * Interface Etat. Définit le comportement implémenté dans les différents états.
 * @author Antonin
 */
public interface Etat {

	/**
	 * Méthode insererCarte. Permet d'insérer une carte dans le distributeur.
	 * @param uneCarte Un objet Carte correspondant à une carte bancaire.
	 */
	void insererCarte(Carte uneCarte);

	/**
	 * Méthode saisirCode. Permet de saisir le code correspondant à la carte dans le distributeur.
	 * @param unCode Chaine de caractère correspondant au code saisi.
	 */
	void saisirCode(String unCode);

	/**
	 * Méthode retirerBillets. Permet de retirer une somme d'argent du distributeur.
	 * @param somme Un entier représenatnt une somme d'argent.
	 */
	void retirerBillets(int somme);

        /**
         * Méthode retirerCarte. Permet de retirer la carte qui est insérée dans le distributeur.
         * @return L'objet Carte qui était inséré dans le distributeur.
         */
	Carte retirerCarte();

        /**
         * Méthode consulterSolde. Permet de consulter le solde d'un compte.
         * @return Un entier représentant le solde du compte.
         */
	int consulterSolde();

	/**
	 * Méthode reapprovisionner. Sert à ajouter une somme d'argent dans les fonds du distributeur.
	 * @param somme Un entier représenatnt une somme d'argent.
	 */
	void reapprovisionner(int somme);

        /**
         * Méthode effectuerRetrait. Permet de choisir l'opération de retrait d'argent.
         */
	void effectuerRetrait();

        /**
         * Méthode getMessage. Sert à connaitre l'état actuel du distributeur.
         * @return Une chaine de caractère correspondant à l'état du distributeur.
         */
	String getMessage();

}
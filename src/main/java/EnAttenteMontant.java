import banque.*;

/**
 * Classe EnAttenteMontant. Implémente l'état où le distributeur attend qu'un montant soit entré.
 * @author Antonin
 */
public class EnAttenteMontant implements Etat {

	private Distributeur monDistributeur;

	/**
	 * Constructeur de la classe EnAttenteMontant.
	 * @param unDistributeur
	 */
	public EnAttenteMontant(Distributeur unDistributeur) {
            this.monDistributeur = unDistributeur;
	}

        @Override
	public void insererCarte(Carte uneCarte) {
            throw new IllegalStateException("Vous ne pouvez pas insérer plus d'une carte dans le distributeur.");
	}

        @Override
	public void saisirCode(String unCode) {
            throw new IllegalStateException("Votre code est déjà saisi, veuillez choisir un montant à retirer.");
	}

        @Override
	public void retirerBillets(int somme) {
            
            int soldeClient = this.monDistributeur.getCarte().getCompte().getSolde();
            
            if (soldeClient >= somme) {
                String retraitPartiel = "";     //sert a ajouter une explication au retrait partiel
                if (this.monDistributeur.getFonds() < somme) {
                    somme = this.monDistributeur.getFonds();
                    retraitPartiel = "En raison d'un manque de fonds, un retrait partiel a été effectué. Merci de votre compréhension.";
                }
                //retirer somme dans le distributeur
                this.monDistributeur.enleverBillets(somme);
                //retirer somme sur compte client
                this.monDistributeur.getCarte().getCompte().debiter(somme);
                //changement état
                this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteOperation());
                System.out.println("La somme de " + somme + "€ a été retirée. " + retraitPartiel);
            }
	}

        @Override
	public Carte retirerCarte() {
            throw new IllegalStateException("Vous ne pouvez pas retirer votre carte, veuillez choisir un montant à retirer.");
	}

        @Override
	public int consulterSolde() {
            throw new IllegalStateException("Vous ne pouvez pas consulter votre solde actuellement, veuillez choisir un montant à retirer.");
	}

        @Override
	public void reapprovisionner(int somme) {
            throw new IllegalStateException("Vous ne pouvez pas réapprovisionner le distributeur car une carte est insérée.");
	}

        @Override
	public void effectuerRetrait() {
            throw new IllegalStateException("Vous êtes déjà en train de faire un retrait.");
	}

        @Override
	public String getMessage() {
            return "→ Etat distributeur : " + this.getClass().getSimpleName();
	}

}
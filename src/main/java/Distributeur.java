import banque.*;
import java.util.ArrayList;

/**
 * Classe Distributeur. Contient les méthodes qui permettent de manipuler ses états et ses différentes interactions possibles.
 * @author Antonin
 */
public class Distributeur {
    private Carte carte;
    private ArrayList<Carte> cartesAvalees;

    private Etat etatEnAttenteOperation;
    private Etat etatEnAttenteCode;
    private Etat etatHorsService;
    private Etat etatEnAttenteMontant;
    private Etat etatEnAttenteCarte;
    private int fonds;
    
    private Etat monEtat;

    /**
     * Constructeur de la classe Distributeur.
     * @param fonds Entier représentant la somme d'argent initiale contenue par le distributeur
     */
    public Distributeur(int fonds) {
        this.fonds = fonds;
        this.carte = null;
        this.cartesAvalees = new ArrayList<Carte>();
        this.etatEnAttenteOperation = new EnAttenteOperation(this);
        this.etatEnAttenteCode = new EnAttenteCode(this);
        this.etatHorsService = new HorsService(this);
        this.etatEnAttenteMontant = new EnAttenteMontant(this);
        this.etatEnAttenteCarte = new EnAttenteCarte(this);
        
        //Si le distributeur ne contient pas d'argent à son initialisation,
        //il ne faut pas le mettre dans son état de service normal.
        if (this.fonds <= 0) {
            this.setEtat(etatHorsService);
        }else{
            this.setEtat(etatEnAttenteCarte);
        }
    }

    
    /**
     * Méthode getFonds. Getter de l'attibut fonds du Distributeur.
     * @return Un entier représentant la somme d'argent actuellement dans le Distributeur.
     */
    public int getFonds() {
        return fonds;
    }

    /**
     * Méthode getCarte. Getter de l'attibut carte du Distributeur.
     * @return Objet de type Carte correspondant à la carte actuellement dans le Distributeur.
     */
    Carte getCarte() {
        return this.carte;
    }

    /**
     * Méthode setCarte. Setter de l'attibut carte du Distributeur.
     * @param uneCarte Objet de type Carte que l'on affecte à l'attribut carte du Distributeur.
     */
    void setCarte(Carte uneCarte) {
        this.carte = uneCarte;
    }

    /**
     * Méthode setEtat. Setter de l'attibut monEtat du Distributeur.
     * @param unEtat Un objet de type Etat représentant l'état auquel on veut placer le Distributeur.
     */
    void setEtat(Etat unEtat) {
        this.monEtat = unEtat;
    }

    /**
     * Méthode enleverBillets. Permet d'enlever une somme d'argent du Distributeur.
     * @param somme Un entier représentant une somme d'argent.
     */
    //il faudrait protéger cette méthode pour qu'on ne puisse pas enlever de l'argent sans passer par l'état EnAttenteOperation
    //car si le Distributeur est vidé entièrement manuellement (à partir du main() par ex.), le distributeur ne serait pas HorsService.
    int enleverBillets(int somme) {
        this.fonds -= somme;
        //Retrait partiel prit en compte dans retirerBillets()
        return somme;
    }

    /**
     * Méthode ajouterBillets. Permet d'ajouter une somme d'argent dans le Distributeur.
     * @param somme Un entier représentant une somme d'argent.
     */
    void ajouterBillets(int somme) {
        this.fonds += somme;
    }

    /**
     * Méthode estVide. Permet de savoir si le Distributeur est vide en consultant la valeur de l'attibut fonds du Distributeur.
     * @return Vrai si le distributeur est vide, Faux sinon.
     */
    boolean estVide() {
        return this.fonds <= 0;
    }
    
    

    /**
     * Méthode reapprovisionner. Sert à appeler la méthode de l'Etat monEtat qui sert à ajouter une somme d'argent dans les fonds du distributeur.
     * @param somme Un entier représentant une somme d'argent.
     */
    public void reapprovisionner(int somme) {
        this.monEtat.reapprovisionner(somme);
    }

    /**
     * Méthode insererCarte. Permet d'appeler la méthode de l'Etat monEtat qui permet d'insérer une carte dans le distributeur.
     * @param uneCarte Objet de type Carte que l'on insère dans le Distributeur.
     */
    public void insererCarte(Carte uneCarte) {
        //la "détection" de cartes déjà avalées ne marche pas :
        if (this.cartesAvalees.contains(uneCarte)) {
            System.out.println("Impossible d'insérer cette carte, elle a déjà été avalée.");
        }else{
            this.monEtat.insererCarte(uneCarte);
        }
    }

    /**
     * Méthode saisirCode. Permet d'appeler la méthode de l'Etat monEtat qui permet de saisir le code correspondant à la carte dans le distributeur.
     * @param unCode Chaine de caractère correspondant au code saisi.
     */
    public void saisirCode(String unCode) {
        this.monEtat.saisirCode(unCode);
    }

    /**
     * Méthode retirerBillets. Permet d'appeler la méthode de l'Etat monEtat qui permet de retirer une somme d'argent du distributeur.
     * @param somme Un entier représentant une somme d'argent.
     */
    public void retirerBillets(int somme) {
        this.monEtat.retirerBillets(somme);
    }

    /**
     * Méthode retirerCarte. Permet d'appeler la méthode de l'Etat monEtat qui permet de retirer la carte qui est insérée dans le distributeur.
     * @return L'objet Carte qui était inséré dans le distributeur.
     */
    public Carte retirerCarte() {
        return this.monEtat.retirerCarte();
        //.setCarte(null);      ici ou dans méthode ??
    }

    /**
     * Méthode consulterSolde. Permet d'appeler la méthode de l'Etat monEtat qui permet de consulter le solde d'un compte.
     * @return Un entier représentant le solde du compte.
     */
    public int consulterSolde() {
        return this.monEtat.consulterSolde();
    }

    /**
     * Méthode avalerCarte. Permet de d'avaler une carte qui a été bloquée par la méthode saisirCode().
     */
    void avalerCarte() {
        //complet ??
        if (this.cartesAvalees.contains(this.carte)) {
            System.err.println("La carte a déjà été avalée");
        }else{
            this.cartesAvalees.add(this.carte);
            System.out.println("La carte a été avalée suite à 3 echecs consécutifs. Merci de votre compréhension.");
            this.setCarte(null);
        }
    }

    /**
     * Méthode effectuerRetrait. Permet d'appeler la méthode de l'Etat monEtat qui permet de choisir l'opération de retrait d'argent.
     */
    public void effectuerRetrait() {
        this.monEtat.effectuerRetrait();
    }

    /**
     * Méthode afficherEtat. Permet d'afficher l'état actuel du distributeur.
     */
    public void afficherEtat() {
        System.out.println(this.getMessage());
    }

    /**
    * Méthode getMessage. Permet d'appeler la méthode de l'Etat monEtat qui permet de connaitre l'état actuel du distributeur.
    * @return Une chaine de caractère correspondant à l'état du distributeur.
    */
    public String getMessage() {
        return this.monEtat.getMessage();
    }        

    /**
    * Méthode getEtatEnAttenteOperation. Getter de l'attibut etatEnAttenteOperation du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut etatEnAttenteOperation du Distributeur.
    */
    public Etat getEtatEnAttenteOperation() {
        return etatEnAttenteOperation;
    }

    /**
    * Méthode getEtatEnAttenteCode. Getter de l'attibut etatEnAttenteCode du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut etatEnAttenteCode du Distributeur.
    */
    public Etat getEtatEnAttenteCode() {
        return etatEnAttenteCode;
    }

    /**
    * Méthode getEtatHorsService. Getter de l'attibut etatHorsService du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut etatHorsService du Distributeur.
    */
    public Etat getEtatHorsService() {
        return etatHorsService;
    }

    /**
    * Méthode getEtatEnAttenteMontant. Getter de l'attibut etatEnAttenteMontant du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut etatEnAttenteMontant du Distributeur.
    */
    public Etat getEtatEnAttenteMontant() {
        return etatEnAttenteMontant;
    }

    /**
    * Méthode getEtatEnAttenteCarte. Getter de l'attibut etatEnAttenteCarte du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut etatEnAttenteCarte du Distributeur.
    */
    public Etat getEtatEnAttenteCarte() {
        return etatEnAttenteCarte;
    }

    /**
    * Méthode getMonEtat. Getter de l'attibut monEtat du Distributeur.
    * @return Objet de type Etat correspondant à l'attribut monEtat du Distributeur.
    */
    //méthode ajoutée pour les Tests Unitaires :
    public Etat getMonEtat() {
        return monEtat;
    }
}

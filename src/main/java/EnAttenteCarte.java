import banque.*;

/**
 * Classe EnAttenteCarte. Implémente l'état où le distributeur attend qu'une carte soit insérée.
 * @author Antonin
 */
public class EnAttenteCarte implements Etat {

	private Distributeur monDistributeur;

	/**
	 * Constructeur de la classe EnAttenteCarte.
	 * @param unDistributeur
	 */
	public EnAttenteCarte(Distributeur unDistributeur) {
            this.monDistributeur = unDistributeur;
	}

        @Override
	public void insererCarte(Carte uneCarte) {
            //on ne peut pas insérer une carte déjà avalée -> pris en compte dans insererCarte() de Distributeur
            
            if (this.monDistributeur.getCarte() != null) {
                System.err.println("Il y a déjà une carte insérée dans le distributeur.");
            }else{
                this.monDistributeur.setCarte(uneCarte);
                this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteCode());
            }
	}

        @Override
	public void saisirCode(String unCode) {
            throw new IllegalStateException("Veuillez insérer une carte avant de saisir un code.");
	}

        @Override
	public void retirerBillets(int somme) {
            throw new IllegalStateException("Veuillez insérer une carte et entrer votre code pour effectuer un retrait.");
	}

        @Override
	public Carte retirerCarte() {
            throw new IllegalStateException("Veuillez insérer une carte avant d'essayer de la retirer.");
	}

        @Override
	public int consulterSolde() {
            throw new IllegalStateException("Veuillez d'abord insérer une carte avant de saisir consulter le solde.");
	}

        @Override
	public void reapprovisionner(int somme) {
            //ici on ne change pas d'état
            this.monDistributeur.ajouterBillets(somme);
	}

        @Override
	public void effectuerRetrait() {
            throw new IllegalStateException("Veuillez insérer une carte et entrer votre code avant d'effectuer un retrait.");
	}

        @Override
	public String getMessage() {
            return "→ Etat distributeur : " + this.getClass().getSimpleName();
	}

}
package banque;

/**
 * Classe Carte. Représente la carte bancaire affiliée au compte d'un client.
 * @author Antonin
 */
public class Carte {

    private Compte compte;
    private String code;
    public static final int NB_TENTATIVES_MAX = 3;
    private int nbEchecs;

    /**
     * Constructeur de la classe Carte.
     * @param unCode Chaine de caractère représentant le code de la carte bancaire.
     * @param unCompte Objet Compte représentant le compte lié à la carte bancaire.
     */
    public Carte(String unCode, Compte unCompte) {
        this.code = unCode;
        this.compte = unCompte;
    }

    /**
     * Méthode verifierCode. Permet de vérifier la véracité d'un code saisit.
     * @param unCode Chaine de caractère représentant le code de la carte bancaire à vérifier.
     * @return Vrai si le code saisi est juste, Faux sinon.
     */
    public boolean verifierCode(String unCode) {
        return this.code.equals(unCode);
    }

    /**
     * Méthode getCompte. Getter de l'attribut compte.
     * @return Un objet Compte représentant le compte lié à la carte bancaire.
     */
    public Compte getCompte() {
        return this.compte;
    }

    /**
     * Méthode ajouteEchec. Incrémente le nombre d'echecs de la carte.
     */
    public void ajouteEchec() {
        this.nbEchecs++;
    } 

    /**
     * Méthode getNbEchecs. 
     * @return Un entier représentant le nombre d'echecs actuel de la carte.
     */
    public int getNbEchecs() {
        return nbEchecs;
    }        

    /**
     * Méthode estBloquee. Cette méthode n'est pas définie
     * @return Vrai si la carte est bloquée, Faux si elle ne l'est pas.
     */
    public boolean estBloquee() {

        //impossible de faire monDistrib.cartesAvalees.contains()  :/

        // TODO - implement Carte.estBloquee
        throw new UnsupportedOperationException();
    }

    /**
     * Méthode resetNbEchecs. Remet à zéro le nombre d'echecs de la carte.
     */
    public void resetNbEchecs() {
        this.nbEchecs = 0;
    }


}

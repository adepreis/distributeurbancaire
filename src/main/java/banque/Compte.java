package banque;

/**
 * Classe Compte. Représente le compte bancaire d'un client.
 * @author Antonin
 */
public class Compte {
    //faire une methode ajouterArgent ?
    private int solde;

    /**
     * Constructeur de la classe Compte.
     * @param soldeInitial
     */
    public Compte(int soldeInitial) {
        this.solde = soldeInitial;
    }

    /**
     * Méthode getSolde(). Getter de l'attribut solde.
     * @return Un entier représentant le solde du compte.
     */
    public int getSolde() {
        return this.solde;
    }

    /**
     * Méthode debiter(). Soustrait le montant retiré au solde du compte.
     * @param montant Un entier représentant le montant à débiter.
     */
    public void debiter(int montant) {
        this.solde -= montant;
    }

}

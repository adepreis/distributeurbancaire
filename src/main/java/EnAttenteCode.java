import banque.*;

/**
 * Classe EnAttenteCode. Implémente l'état où le distributeur attend que le code de la carte soit saisi.
 * @author Antonin
 */
public class EnAttenteCode implements Etat {

	private Distributeur monDistributeur;

	/**
	 * Constructeur de la classe EnAttenteCode.
	 * @param unDistributeur
	 */
	public EnAttenteCode(Distributeur unDistributeur) {
            this.monDistributeur = unDistributeur;
	}

        @Override
	public void insererCarte(Carte uneCarte) {
            throw new IllegalStateException("Vous ne pouvez pas insérer plus d'une carte dans le distributeur.");
	}

        @Override
	public void saisirCode(String unCode) {
            
            //eventuellement faire des diclaimers : "x essais restants", "2 echecs consécutifs"
            
            boolean codeValide = this.monDistributeur.getCarte().verifierCode(unCode);
            if (!codeValide) {
                this.monDistributeur.getCarte().ajouteEchec();
                
                //?? Gérer l'acces au NB_MAX et au nbEchecs dans Carte :
                if (this.monDistributeur.getCarte().getNbEchecs() >= Carte.NB_TENTATIVES_MAX) {
                    this.monDistributeur.avalerCarte();
                    this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteCarte());
                }
                
            }else{
                //si code valide on reset le nbEchec à 0 et on change d'état
                this.monDistributeur.getCarte().resetNbEchecs();
                this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteOperation());
            }
	}

        @Override
	public void retirerBillets(int somme) {
            throw new IllegalStateException("Veuillez entrer votre code pour effectuer un retrait.");
	}

        @Override
	public Carte retirerCarte() {
            //a implementer
            System.out.println("A Implementer/compléter : ordre des get/set : renvoi null ??");
            
            this.monDistributeur.setCarte(null); //???
            
            this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteCarte());
            
            return this.monDistributeur.getCarte(); //???
        }

        @Override
	public int consulterSolde() {
            throw new IllegalStateException("Assurez vous d'avoir entré votre code avant de consulter votre solde.");
	}

        @Override
	public void reapprovisionner(int somme) {
            throw new IllegalStateException("Vous ne pouvez pas réapprovisionner le distributeur car une carte est insérée.");
	}

        @Override
	public void effectuerRetrait() {
            throw new IllegalStateException("Veuillez entrer votre code avant d'effectuer un retrait.");
	}

        @Override
	public String getMessage() {
            return "→ Etat distributeur : " + this.getClass().getSimpleName();
	}

}
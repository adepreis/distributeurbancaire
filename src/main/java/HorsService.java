import banque.*;

/**
 * Classe HorsService. Implémente l'état où le distributeur est Hors Service. On ne peut alors que le réapprovisionner.
 * @author Antonin
 */
public class HorsService implements Etat {

	private Distributeur monDistributeur;

        /**
         * Constructeur de la classe HorsService.
         * @param unDistributeur 
         */
	public HorsService(Distributeur unDistributeur) {
            this.monDistributeur = unDistributeur;
	}

        
        @Override
	public void insererCarte(Carte uneCarte) {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        
        @Override
	public void saisirCode(String unCode) {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        
        @Override
	public void retirerBillets(int somme) {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        
        @Override
	public Carte retirerCarte() {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        @Override
	public int consulterSolde() {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        
        @Override
	public void reapprovisionner(int somme) {
            this.monDistributeur.ajouterBillets(somme);
            this.monDistributeur.setEtat(this.monDistributeur.getEtatEnAttenteCarte());
	}

        @Override
	public void effectuerRetrait() {
            throw new IllegalStateException("Distributeur hors service. De plus, aucune carte n'est insérée.");
	}

        @Override
	public String getMessage() {
            return "→ Etat distributeur : " + this.getClass().getSimpleName();
	}

}
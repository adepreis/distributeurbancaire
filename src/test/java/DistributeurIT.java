// BookMark UnitTest et IT :
        //  https://stackoverflow.com/questions/7797816/how-to-unit-test-a-state-machine
        //  https://www.planetgeek.ch/2011/05/17/how-to-unit-test-finite-state-machines/
        //  https://youtu.be/hBCaoN421Qs -->Xavier Blanc, Comment tester ?
        //  https://www.spiria.com/fr/blogue/web-applications/comprendre-la-difference-entre-un-test-unitaire-et-un-test-dintegration
        //  assertSame(Str message, Obj expected, Obj actual);   --> permet de vérifier qu'un élément y est une instance de la classe x.
        //  fail(Str msg);          --> pour alerter si non déclenchement d'exception (cours5 BCOO2)

import banque.Carte;
import banque.Compte;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de test d'intégration DistributeurIT.
 * @author Antonin
 */
public class DistributeurIT {
    private Distributeur distrib;

    public DistributeurIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Méthode setUp(). Permet d'initialiser l'attribut distrib avant chaque test.
     */
    @Before
    public void setUp() {
        this.distrib = new Distributeur(5000);
        //création de compte + carte au début de chaque test
    }
    
    /**
     * Méthode tearDown(). Permet de "nettoyer" l'attribut distrib à la fin de chaque test.
     */
    @After
    public void tearDown() {
        this.distrib = null;
    }

    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_CARTE •••••
    
    /**
     * Test of SaisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testSaisirCodeEnAttenteCarte() {
        System.out.println("saisirCodeEnAttenteCarte");
        
        distrib.setEtat(distrib.getEtatEnAttenteCarte());
        
        try {
            distrib.saisirCode("1234");
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of effectuerRetrait method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testEffectuerRetraitEnAttenteCarte() {
        System.out.println("effectuerRetraitEnAttenteCarte");
        
        distrib.setEtat(distrib.getEtatEnAttenteCarte());
        
        try {
            distrib.effectuerRetrait();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
        }
    }

    /**
     * Test of consulterSolde method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testConsulterSoldeEnAttenteCarte() {
        System.out.println("consulterSoldeEnAttenteCarte");
        
        distrib.setEtat(distrib.getEtatEnAttenteCarte());
        
        try {
            distrib.consulterSolde();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testRetirerCarteEnAttenteCarte() {
        System.out.println("retirerCarteEnAttenteCarte");
        
        distrib.setEtat(distrib.getEtatEnAttenteCarte());
        
        try {
            distrib.retirerCarte();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de la carte à un mauvais état", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerBillets method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testRetirerBilletsEnAttenteCarte() {
        System.out.println("retirerBilletsEnAttenteCarte");
        
        distrib.setEtat(distrib.getEtatEnAttenteCarte());
        
        try {
            distrib.retirerBillets(500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de billets à un mauvais état", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
        }
    }
 
    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_CODE •••••
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testInsererCarteEnAttenteCode() {
        System.out.println("insererCarteEnAttenteCode");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.setEtat(distrib.getEtatEnAttenteCode());
        
        try {
            distrib.insererCarte(carte);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Insertion de la carte à un mauvais état", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of effectuerRetrait method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testEffectuerRetraitEnAttenteCode() {
        System.out.println("effectuerRetraitEnAttenteCode");
        
        distrib.setEtat(distrib.getEtatEnAttenteCode());
        
        try {
            distrib.effectuerRetrait();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
        }
    }

    /**
     * Test of consulterSolde method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testConsulterSoldeEnAttenteCode() {
        System.out.println("consulterSoldeEnAttenteCode");
        
        distrib.setEtat(distrib.getEtatEnAttenteCode());
        
        try {
            distrib.consulterSolde();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerBillets method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testRetirerBilletsEnAttenteCode() {
        System.out.println("retirerBilletsEnAttenteCode");
        
        distrib.setEtat(distrib.getEtatEnAttenteCode());
        
        try {
            distrib.retirerBillets(500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de billets à un mauvais état", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of reapprovisionner method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testReapprovisionnerEnAttenteCode() {
        System.out.println("reapprovisionnerDistributeurEnAttenteCode");
        
        distrib.setEtat(distrib.getEtatEnAttenteCode());
        
        try {
        distrib.reapprovisionner(6500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Reapprovisionnement du distributeur  à un mauvais état", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
        }
    }

    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_OPERATION •••••
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testInsererCarteEnAttenteOperation() {
        System.out.println("insererCarteEnAttenteOperation");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.setEtat(distrib.getEtatEnAttenteOperation());
        
        try {
            distrib.insererCarte(carte);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Insertion de la carte à un mauvais état", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of SaisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testSaisirCodeEnAttenteOperation() {
        System.out.println("saisirCodeEnAttenteOperation");
        
        distrib.setEtat(distrib.getEtatEnAttenteOperation());
        
        try {
            distrib.saisirCode("1234");
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerBillets method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testRetirerBilletsEnAttenteOperation() {
        System.out.println("retirerBilletsEnAttenteOperation");
        
        distrib.setEtat(distrib.getEtatEnAttenteOperation());
        
        try {
            distrib.retirerBillets(500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de billets à un mauvais état", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of reapprovisionner method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testReapprovisionnerEnAttenteOperation() {
        System.out.println("reapprovisionnerDistributeurEnAttenteOperation");
        
        distrib.setEtat(distrib.getEtatEnAttenteOperation());
        
        try {
        distrib.reapprovisionner(6500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Reapprovisionnement du distributeur  à un mauvais état", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
        }
    }
    
   
    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_MONTANT •••••
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testInsererCarteEnAttenteMontant() {
        System.out.println("insererCarteEnAttenteMontant");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
            distrib.insererCarte(carte);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Insertion de la carte à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }

    
    /**
     * Test of SaisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testSaisirCodeEnAttenteMontant() {
        System.out.println("saisirCodeEnAttenteMontant");
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
            distrib.saisirCode("1234");
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of effectuerRetrait method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testEffectuerRetraitEnAttenteMontant() {
        System.out.println("effectuerRetraitEnAttenteMontant");
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
            distrib.effectuerRetrait();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }

    /**
     * Test of consulterSolde method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testConsulterSoldeEnAttenteMontant() {
        System.out.println("consulterSoldeEnAttenteMontant");
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
            distrib.consulterSolde();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testRetirerCarteEnAttenteMontant() {
        System.out.println("retirerCarteEnAttenteMontant");
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
            distrib.retirerCarte();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de la carte à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of reapprovisionner method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testReapprovisionnerEnAttenteMontant() {
        System.out.println("reapprovisionnerDistributeurEnAttenteMontant");
        
        distrib.setEtat(distrib.getEtatEnAttenteMontant());
        
        try {
        distrib.reapprovisionner(6500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Reapprovisionnement du distributeur  à un mauvais état", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
        }
    }

    
    
// ••••• Tests ayant pour ETAT Initial : HORS_SERVICE •••••
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testInsererCarteHorsService() {
        System.out.println("insererCarteHorsService");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.insererCarte(carte);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Insertion de la carte à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of SaisirCode method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testSaisirCodeHorsService() {
        System.out.println("saisirCodeHorsService");
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.saisirCode("1234");
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of effectuerRetrait method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testEffectuerRetraitHorsService() {
        System.out.println("effectuerRetraitHorsService");
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.effectuerRetrait();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }

    /**
     * Test of consulterSolde method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testConsulterSoldeHorsService() {
        System.out.println("consulterSoldeHorsService");
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.consulterSolde();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Saisie d'un code à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testRetirerCarteHorsService() {
        System.out.println("retirerCarteHorsService");
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.retirerCarte();
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de la carte à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }
    
    /**
     * Test of retirerBillets method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testRetirerBilletsHorsService() {
        System.out.println("retirerBilletsHorsService");
        
        distrib.setEtat(distrib.getEtatHorsService());
        
        try {
            distrib.retirerBillets(500);
            fail("C'est bien triste d'en arriver là...");
        } catch (IllegalStateException e) {
            assertSame("Retrait de billets à un mauvais état", this.distrib.getEtatHorsService(), distrib.getMonEtat());
        }
    }
 
}

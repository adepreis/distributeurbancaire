// BookMark UnitTest et IT :
        //  https://stackoverflow.com/questions/7797816/how-to-unit-test-a-state-machine
        //  https://www.planetgeek.ch/2011/05/17/how-to-unit-test-finite-state-machines/
        //  https://youtu.be/hBCaoN421Qs -->Xavier Blanc, Comment tester ?
        //  https://www.spiria.com/fr/blogue/web-applications/comprendre-la-difference-entre-un-test-unitaire-et-un-test-dintegration
        //  assertSame(Str message, Obj expected, Obj actual);   --> permet de vérifier qu'un élément y est une instance de la classe x.
        //  fail(Str msg);          --> pour alerter si non déclenchement d'exception (cours5 BCOO2)

import banque.Carte;
import banque.Compte;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de test unitaires DistributeurTest.
 * @author Antonin
 */
public class DistributeurTest {
    private Distributeur distrib;
    
    public DistributeurTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Méthode setUp(). Permet d'initialiser l'attribut distrib avant chaque test.
     */
    @Before
    public void setUp() {
        this.distrib = new Distributeur(5000);
        //création de compte + carte au début de chaque test
    }
    
    /**
     * Méthode tearDown(). Permet de "nettoyer" l'attribut distrib à la fin de chaque test.
     */
    @After
    public void tearDown() {
        this.distrib = null;
    }
    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_CARTE •••••
    /**
     * Test of reapprovisionner method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testReapprovisionnerDistributeur() {
        System.out.println("- reapprovisionnerDistributeur");
        
        distrib.reapprovisionner(6500);
        assertSame("Reapprovisionnement du distributeur (déjà rempli)", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testInsererCarte() {
        System.out.println("- insererCarte");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        assertSame("Insertion de la carte", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
    }
    
    /**
     * Test of insererCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteCarte.
     */
    @Test
    public void testInsererCarteBloquee() {
        System.out.println("- insererCarteBloquee");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("4321");
        distrib.saisirCode("3630");
        distrib.saisirCode("0000");
        distrib.insererCarte(carte);
        assertSame("Insertion d'une carte bloquée dans le distributeur", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_CODE •••••
    /**
     * Test of saisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testSaisirCode() {
        System.out.println("- saisirCode");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        assertSame("Saisie du code après insertion de la carte", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
    }
    
    /**
     * Test of saisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testSaisirCodeFaux() {
        System.out.println("- saisirCodeFaux");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("4321");
        assertSame("Saisie d'un code faux", this.distrib.getEtatEnAttenteCode(), distrib.getMonEtat());
    }
    
    /**
     * Test of saisirCode method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testSaisirCodeFaux3Fois() {
        System.out.println("- saisirCodeFaux3Fois");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("4321");
        distrib.saisirCode("3630");
        distrib.saisirCode("0000");
        assertSame("Blocage de la carte sur à 3 codes faux", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteCode.
     */
    @Test
    public void testSaisirCodeRetirerCarte() {
        System.out.println("- saisirCodeRetirerCarte");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.retirerCarte();
        assertSame("Retrait de la carte après son insertion", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_OPERATION •••••
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testEtatAttenteOperationRetirerCarte() {
        System.out.println("- etatAttenteOperationRetirerCarte");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.retirerCarte();
        assertSame("Retrait de la carte lors du choix d'opération", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
    /**
     * Test of consulterSolde method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testConsulterSolde() {
        System.out.println("- consulterSolde");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.consulterSolde();
        assertSame("Consultation du solde", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
    }
    
    /**
     * Test of effectuerRetrait method, of class Distributeur, when distributeur.monEtat is EnAttenteOperation.
     */
    @Test
    public void testEffectuerRetrait() {
        System.out.println("- effectuerRetrait");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.effectuerRetrait();
        assertSame("Choix de l'opération retrait", this.distrib.getEtatEnAttenteMontant(), distrib.getMonEtat());
    }
    
    
// ••••• Tests ayant pour ETAT Initial : EN_ATTENTE_MONTANT •••••
    /**
     * Test of retirerBillets method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testRetirerBillets() {
        System.out.println("- retirerBillets");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(2500);  //Compte avec 2500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte1 / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.effectuerRetrait();
        distrib.retirerBillets(800);
        assertSame("Retait d'un montant de 800€", this.distrib.getEtatEnAttenteOperation(), distrib.getMonEtat());
    }
    
    /**
     * Test of retirerCarte method, of class Distributeur, when distributeur.monEtat is EnAttenteMontant.
     */
    @Test
    public void testRetirerCarteDistributeurVide() {
        System.out.println("- retirerCarteDistributeurVide");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(8500);  //Compte avec 8500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.effectuerRetrait();
        distrib.retirerBillets(5000);
        distrib.retirerCarte();
        assertSame("Retait de la carte avec un distributeur de 0€", this.distrib.getEtatHorsService(), distrib.getMonEtat());
    }
    
    
    
// ••••• Tests ayant pour ETAT Initial : HORS_SERVICE •••••
    /**
     * Test of reapprovisionner method, of class Distributeur, when distributeur.monEtat is HorsService.
     */
    @Test
    public void testReapprovisionnerDistributeurVide() {
        System.out.println("- reapprovisionnerDistributeurVide");
        
        //création d'un compte et sa carte :
        Compte compte = new Compte(8500);  //Compte avec 8500€
        Carte carte = new Carte("1234", compte);    //Carte du client possedant le compte / code
        
        distrib.insererCarte(carte);
        distrib.saisirCode("1234");
        distrib.effectuerRetrait();
        distrib.retirerBillets(5000);
        distrib.retirerCarte();
        distrib.reapprovisionner(6500);
        assertSame("Reapprovisionnement du distributeur vide", this.distrib.getEtatEnAttenteCarte(), distrib.getMonEtat());
    }
    
}

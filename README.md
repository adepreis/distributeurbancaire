# DistributeurBancaire

IUT old project for GitLab CI testing.

[![pipeline status](https://gitlab.com/adepreis/distributeurbancaire/badges/master/pipeline.svg)](https://gitlab.com/adepreis/distributeurbancaire/-/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development purpose.

### Prerequisites

Things you need to install the project :

- [Java SE](https://www.java.com/fr/download/)


### Installing

Here are some instructions on how to get the development env running.

First, clone this repository with the following command :

`git clone https://gitlab.com/adepreis/distributeurbancaire`

Then, create a Maven project with existing sources (`/src` folder) on your 
favorite IDE and build the project. The POM file will download the 
dependencies.

### Testing

You don't need to include JUnit 4 library manually, the POM file should
already have done it for you.

The existing unit tests are located in `/src/test/java`.

### Run the latest version

Execute the .jar file from the last [release assets](https://gitlab.com/adepreis/distributeurbancaire/-/releases) with the following command :

`java -jar DistributeurBancaire.jar`
